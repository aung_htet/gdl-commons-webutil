/**
 * FileName      : $Id: ProxiedExceptionHandlerExceptionResolver.java $
 *
 * Copyright Notice: ©2004 Singapore Telecom Pte Ltd -- Confidential and Proprietary
 *
 * All rights reserved.
 * This software is the confidential and proprietary information of SingTel Pte Ltd
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the license agreement you
 * entered into with SingTel.
 */
package au.com.optus.gdl.commons.webutil.exception;

import au.com.optus.gdl.commons.util.aop.AspectUtil;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExceptionHandlerMethodResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;

/**
 * To expose {@link @org.springframework.web.bind.annotation.ExceptionHandler}
 * annotated methods in jdkproxied Controller implementation so that Spring framework can pick up
 * the Error handling logic within the jdkproxied controller class.
 * This ExceptionResolver will be loaded with Spring's DispatcherServlet initialisation
 * and will be used as the first resolver among others.
 *
 */
@Component("au.com.optus.gdl.services.msisdnreader.mvc.ProxiedExceptionHandlerExceptionResolver")
public class ProxiedExceptionHandlerExceptionResolver extends ExceptionHandlerExceptionResolver {

    /**
     * Parent class has this member variable as private. Declaring here again because it is only used
     * with <code>getExceptionHandlerMethod</code>.
     */
    private final Map<Class<?>, ExceptionHandlerMethodResolver> exceptionHandlerCache =
        new ConcurrentHashMap<Class<?>, ExceptionHandlerMethodResolver>(64);

    /**
     * Default Contructor which sets itself infront of all other resolvers.
     */
    public ProxiedExceptionHandlerExceptionResolver() {
        setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ServletInvocableHandlerMethod getExceptionHandlerMethod(final HandlerMethod handlerMethod,
        final Exception exception) {
        if (handlerMethod != null) {
            Class<?> handlerType = null;
            Object controller = handlerMethod.getBean();
            if (AopUtils.isAopProxy(controller)) {
                controller = AspectUtil.unwrapAopProxy(controller);
                handlerType = controller.getClass();
                logger.debug("Found Proxied " + handlerType
                    + ". Detecting any @ExceptionHandler annotated method in it.");
            } else {
                handlerType = handlerMethod.getBeanType();
            }
            ExceptionHandlerMethodResolver resolver = this.exceptionHandlerCache.get(handlerType);
            if (resolver == null) {
                resolver = new ExceptionHandlerMethodResolver(handlerType);
                this.exceptionHandlerCache.put(handlerType, resolver);
            }
            Method method = resolver.resolveMethod(exception);
            if (method != null) {
                return new ServletInvocableHandlerMethod(controller, method);
            }
        }
        return null;
    }

}
